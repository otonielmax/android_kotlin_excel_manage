package com.otoniel.manageexcel.util

import org.apache.poi.ss.usermodel.Row.CREATE_NULL_AS_BLANK
//import javax.swing.UIManager.put
import org.apache.poi.ss.usermodel.Cell
import android.content.ContentValues
import android.util.Log
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Sheet


object ExcelManagement {

    fun getAllRow(sheet: Sheet?) {

        val rit = sheet?.rowIterator()
        if (rit != null) {
            while (rit.hasNext()!!) {
                val row = rit.next()

                //val contentValues = ContentValues()
                row.getCell(0, Row.CREATE_NULL_AS_BLANK).cellType = Cell.CELL_TYPE_STRING
                row.getCell(1, Row.CREATE_NULL_AS_BLANK).cellType = Cell.CELL_TYPE_STRING
                row.getCell(2, Row.CREATE_NULL_AS_BLANK).cellType = Cell.CELL_TYPE_STRING

                Log.e(
                    "ExcelManagment",
                    row.getCell(0, Row.CREATE_NULL_AS_BLANK).stringCellValue + ' ' +
                            row.getCell(1, Row.CREATE_NULL_AS_BLANK).stringCellValue + ' ' +
                            row.getCell(2, Row.CREATE_NULL_AS_BLANK).stringCellValue)

                /*
                contentValues.put(Company, row.getCell(0, Row.CREATE_NULL_AS_BLANK).stringCellValue)
                contentValues.put(Product, row.getCell(1, Row.CREATE_NULL_AS_BLANK).stringCellValue)
                contentValues.put(Price, row.getCell(2, Row.CREATE_NULL_AS_BLANK).stringCellValue)
                 */

                try {
                    /*
                    if (dbAdapter.insert("MyTable1", contentValues) < 0) {
                        return
                    }
                    */
                } catch (ex: Exception) {
                    Log.d("Exception in importing", ex.message.toString())
                }

            }
        }
    }
}
