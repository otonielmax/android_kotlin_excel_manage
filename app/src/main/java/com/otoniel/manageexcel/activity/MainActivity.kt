package com.otoniel.manageexcel.activity

import android.app.Activity
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.otoniel.manageexcel.R

import kotlinx.android.synthetic.main.activity_main.*
import android.content.ActivityNotFoundException
import android.content.Intent
import android.util.Log
import android.view.View
import com.google.android.material.floatingactionbutton.FloatingActionButton

import org.apache.poi.hssf.usermodel.HSSFWorkbook
import org.apache.poi.ss.usermodel.Workbook
import com.otoniel.manageexcel.util.ExcelManagement
import java.io.FileInputStream
import java.io.IOException
import java.io.InputStream
import org.apache.poi.poifs.filesystem.POIFSFileSystem
import org.apache.poi.ss.usermodel.Sheet


class MainActivity : AppCompatActivity() {

    companion object {
        const val REQUEST_CODE_SEARCH_FILE = 111
    }

    lateinit var view : View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        /*
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }
        */
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun onClickSearchFile(v: View) {
        view = v
        val fileintent = Intent(Intent.ACTION_GET_CONTENT)
        fileintent.type = "gagt/sdf"
        try {
            Log.e("Excel", "Recuerda")
            startActivityForResult(fileintent, REQUEST_CODE_SEARCH_FILE)
        } catch (e: ActivityNotFoundException) {
            Log.e("Excel", e.toString())
            showToast(v, "Error al obtener el archivo")
        } catch (e: java.lang.Exception) {
            Log.e("Excel", e.toString())
        }

    }

    fun showToast(v: View, text: String) {
        Snackbar.make(v, text, Snackbar.LENGTH_LONG).show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.e("Excel", "Que es lo que")
        if (data == null)
            return
        when (requestCode) {
            REQUEST_CODE_SEARCH_FILE -> {
                val FilePath = data.data!!.path
                try {
                    if (resultCode == Activity.RESULT_OK) {
                        var inStream: InputStream? = null
                        var wb: Workbook?
                        var sheet1: Sheet? = null
                        try {
                            inStream = FileInputStream(FilePath)
                            val poifs = POIFSFileSystem(inStream)
                            wb = HSSFWorkbook(poifs)
                            sheet1 = wb.getSheetAt(0)
                        } catch (e: IOException) {
                            showToast(view, "Error al leer el archivo")
                            //lbl.setText("First " + e.getMessage().toString())
                            e.printStackTrace()
                            Log.e("Excel", e.toString())
                        }

                        //val sheet2 = wb.getSheetAt(1)
                        if (sheet1 == null) {
                            try {
                                inStream = FileInputStream(FilePath)
                                wb = HSSFWorkbook(inStream)
                                sheet1 = wb.getSheetAt(0)
                            } catch (e: IOException) {
                                showToast(view, "Error al leer el archivo")
                                //lbl.setText("First " + e.getMessage().toString())
                                e.printStackTrace()
                                Log.e("Excel", e.toString())
                            }
                        }
                        /*
                        if (sheet2 == null) {
                            return
                        }
                        */

                        if (inStream != null) {
                            inStream.close()
                        }
                        ExcelManagement.getAllRow(sheet1)

                    }
                } catch (ex: Exception) {
                    showToast(view, "Error al obtener el archivo")
                    Log.e("Excel", ex.toString())
                }
            }
        }
    }

}
